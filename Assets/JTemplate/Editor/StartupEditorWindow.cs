using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

[InitializeOnLoad]
public class StartupEditorWindow : EditorWindow
{
    [SerializeField]
    private VisualTreeAsset _visualTreeAsset;
    private static bool dontShowAgain = false;
    
    [MenuItem("Tools/JTemplate/Startup Editor Window")]
    public static void ShowWindow()
    {
        var window = GetWindow<StartupEditorWindow>();
        window.titleContent = new GUIContent("Startup Editor Window");
    }

    private void CreateGUI()
    {
        _visualTreeAsset.CloneTree(rootVisualElement);
    }
}
